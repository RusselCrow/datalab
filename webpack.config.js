const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const less = require('./webpack/less');
const css = require('./webpack/css');
const extractCSS = require('./webpack/css.extract');
const uglifyJS = require('./webpack/js.uglify');
const images = require('./webpack/images');
const fonts = require('./webpack/fonts');
//const createPage = require('./webpack/createPage');
const pages=['index'];

const PATHS = {
    source: path.join(__dirname, 'app'),
    build: path.join(__dirname, 'dist'),
};

const common = merge([
    {
        entry: {
            // function(){
            //     var entryObj={};
            //     for (var page1 in pages){
            //         page1=pages[page1];
            //         entryObj[page1]=PATHS.source + '/pages/'+page1+'/'+page1+'.js';
            //     }
            //     return entryObj;
            // },
            akvatoria: PATHS.source + '/pages/projects/akvatoria/akvatoria.js',
            convert: PATHS.source + '/pages/projects/convert/convert.js',
            index: PATHS.source + '/pages/index/index.js',
            services: PATHS.source + '/pages/services/services.js',
            terminals: PATHS.source + '/pages/projects/terminals/terminals.js',
            // akvatoria_dev: [ PATHS.source + '/pages/projects/akvatoria/akvatoria.js', 'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://0.0.0.0:3000'],
            // convert_dev: [ PATHS.source + '/pages/projects/convert/convert.js', 'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://0.0.0.0:3000'],
            // terminals_dev: [ PATHS.source + '/pages/projects/terminals/terminals.js', 'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://0.0.0.0:3000'],
            // index_dev: [ PATHS.source + '/pages/index/index.js', 'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://0.0.0.0:3000'],
            // services_dev: [PATHS.source + '/pages/services/services.js', 'webpack/hot/only-dev-server', 'webpack-dev-server/client?http://0.0.0.0:3000']
        },
        output: {
            path: PATHS.build,
            filename: 'js/[name].js'
        },
        plugins: [
            new HtmlWebpackPlugin({
                filename: 'index.html',
                chunks: ['index'],
                template: PATHS.source + '/pages/index/index.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'convert.html',
                chunks: ['convert'],
                template: PATHS.source + '/pages/projects/convert/convert.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'terminals.html',
                chunks: ['terminals'],
                template: PATHS.source + '/pages/projects/terminals/terminals.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'services.html',
                chunks: ['services'],
                template: PATHS.source + '/pages/services/services.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'akvatoria.html',
                chunks: ['akvatoria'],
                template: PATHS.source + '/pages/projects/akvatoria/akvatoria.pug'
            }),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            })
        ]
    },
    pug(),
    images(),
    fonts()
]);

module.exports = function(env) {
    if (env === 'production'){
        return merge([
            common,
            extractCSS(),
            uglifyJS()
        ])
    }
    if (env === 'development'){
        return merge([
            common,
            devserver(),
            sass(),
            less(),
            css()
        ])
    }
};

function createPage(name){
    return new HtmlWebpackPlugin({
        filename: name+'.html',
        chunks: [name, 'common'],
        template: PATHS.source + '/pages/'+name+'/'+name+'.pug'
    })
}
