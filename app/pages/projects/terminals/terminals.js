import './terminals.less';
import Flickity from 'flickity';

document.addEventListener('DOMContentLoaded', function() {
    setTimeout(function(){
        new Flickity( '#terminals', {
            draggable: false,
            selectedAttraction: 0.01,
            friction: 0.15,
            adaptiveHeight: false,
            initialIndex: 3,
            freeScroll: true,
            wrapAround: true,
            pageDots: true,
            prevNextButtons: true
        });
    
        new Flickity('#terminalsBig', {
            draggable: false,
            pageDots: true,
            freeScroll: true,
            wrapAround: true,
            adaptiveHeight: false,
            prevNextButtons: true,
            lazyLoad: true
        });
        
        new Flickity( '#projects', {
            prevNextButtons: false,
            initialIndex: 0,
            contain: true,
            draggable: true
        });
    }, 100)
});
