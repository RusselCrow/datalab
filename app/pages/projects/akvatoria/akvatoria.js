import './akvatoria.less';
import Flickity from 'flickity';
import 'flickity-as-nav-for';

document.addEventListener('DOMContentLoaded', function() {
    setTimeout(function() {
        new Flickity( '#akvatoriaBig', {
            adaptiveHeight: false,
            selectedAttraction: 0.1,
            friction: 0.8,
            initialIndex: 2,
            draggable: false,
            freeScroll: true,
            wrapAround: true
        });
        new Flickity( '#akvatoria', {
            asNavFor: '#akvatoriaBig',
            selectedAttraction: 0.1,
            draggable: false,
            friction: 0.8,
            adaptiveHeight: true,
            initialIndex: 0,
            freeScroll: true,
            wrapAround: true,
            pageDots: false,
            prevNextButtons: false
        });
        new Flickity( '#projects', {
                prevNextButtons: false,
                initialIndex: 0,
                contain: true,
                draggable: true
            }
        );
        
    }, 100)
});
