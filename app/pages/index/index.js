import './index.less';
import Flickity from 'flickity';
// import '../../libs/jquery-ui-1.12.1.custom/jquery-ui';
// import '../../libs/jquery-ui-1.12.1.custom/jquery-ui.css';
import '../../libs/mask';
import '../../libs/slick.min.js';
import '../../components/header/header.js'


$(window).on('load resize', function() {
	if ($(window).width() < 992) {
		$('.main__mobile-slider').slick({
			arrows: false,
			responsive: [
				{
				  breakpoint: 992,
				  settings: {
					autoplay: true,
					autoplaySpeed: 3000,
					speed: 1000
				  }
				}
				
			  ]
		});
	}
});


// var flkty = new Flickity( '.main-carousel', {
// 		// wrapAround: true,
// 		// autoPlay: 5000,
// 		prevNextButtons: false,
// 		// initialIndex: 1,
// 		contain: true
// 	});

$(window).scroll(function() {
	if( $(this).scrollTop() > 100.0 && $(window).width() > '991') {
		$('.header').addClass('sticky');
		$('.sticky-wrap').addClass('header-wrap_sticky');
	} else {
		$('.header').removeClass('sticky');
		$('.sticky-wrap').removeClass('header-wrap_sticky');
	}
});
$(".phone_mask").mask("+7(999)999-99-99");

// POPUP ANIMATION
function popupAnimation(idBtn, idPopup){
	$(document).ready(function () {

		$(idBtn).click(function () {
			$(idPopup).show({
				duration: 500
			});
			
		});
		
		$(document).mouseup(function (e){ 
			var div = $(".popup"); 
			if (!div.is(e.target) && div.has(e.target).length === 0) {
				$(idPopup).hide(500); 
			}
		});
	
	});
}
popupAnimation('#main-btn', '#main-popup');
popupAnimation('#service-btn', '#service-popup');

