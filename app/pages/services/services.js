import './services.less'
import 'bootstrap'
$(window).scroll(function() {
	if( $(this).scrollTop() > 100 ) {
		$('.header').addClass('sticky');
	} else {
		$('.header').removeClass('sticky');
	}
});
$(window).scroll(function() {
	if( $(this).scrollTop() < 50) {
		const y = $('#atm').offset().top;
		$('.info-table').addClass('info-table-absolute');
		$('.info-table').removeClass('info-table-fixed');
		$('.info-table').removeClass('info-table-start');
		$('.info-table-absolute').offset({top:y+160});
	} else if( $(this).scrollTop() > $('#support').offset().top) {
		const y = $('#support').offset().top;
		$('.info-table').addClass('info-table-absolute');
		$('.info-table').removeClass('info-table-fixed');
		$('.info-table-absolute').offset({top:y+160});
	} else {
		$('.info-table').removeAttr('style');
		$('.info-table').removeClass('info-table-absolute');
		$('.info-table').removeClass('info-table-start');
		$('.info-table').addClass('info-table-fixed');
	}
});
$(window).scroll(function() {
	checkPosition('#atm-link', '#atm', '#statistics');
	checkPosition('#statistics-link', '#statistics', '#api-integration');
	checkPosition('#api-integration-link', '#api-integration', '#high-level-services');
	checkPosition('#high-level-services-link', '#high-level-services', '#stack-technologies');
	checkPosition('#stack-technologies-link', '#stack-technologies', '#ux-development');
	checkPosition('#ux-development-link', '#ux-development', '#web-aplications');
	checkPosition('#web-aplications-link', '#web-aplications', '#support');
	checkPosition('#support-link', '#support', '.footer');
});
function checkPosition(elem, topElem, bottomElem) {
	const elemPosition = $('.info-table').offset().top;
	const topElemPosition = $(topElem).offset().top;
	const bottomElemPosition = $(bottomElem).offset().top;
	if (elemPosition > topElemPosition && elemPosition <= bottomElemPosition) {
		$(elem).addClass('select-Area');
	}
	else {
		$(elem).removeClass('select-Area');
	}
}