// select language
$(document).ready(function () {
	var lngList = document.querySelector('#lng-list');
	var selectLng = document.querySelector('.lng-list__select');
	
	lngList.addEventListener('click', function(e){
		if (e.target.innerText.length < 8) selectLng.textContent = e.target.innerText;
	});
	
});

$(document).ready(function() {
	$('#burger').click(function(){
		$('.header__burger-items').toggleClass('cross');
		$('.mobile-menu').toggleClass('block');
	});
});